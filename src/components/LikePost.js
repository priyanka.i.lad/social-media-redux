import React from "react";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../redux/allActions";

export default function LikePost(props) {
  let { post } = props;

  let dispatch = useDispatch();

  const {
    likedPosts,
    likeCount,
    likedSuccess: successMessage,
    postId,
    token
  } = useSelector(
    ({
      toggleLikePost: { likedPosts, likeCount, likedSuccess, postId },
      login: { token }
    }) => ({
      likedPosts,
      likeCount,
      likedSuccess,
      postId,
      token
    })
  );

  let likedIcon = <i className="fa fa-thumbs-up" aria-hidden="true"></i>;
  let unlikedIcon = <i className="fa fa-thumbs-o-up" aria-hidden="true"></i>;

  return (
    <>
      <button
        onClick={() =>
          dispatch(
            allActions.toggleLikePostActions.toggleLikePost(
              "/user/post/toggle-like",
              post._id,
              token
            )
          )
        }
        className="btn btn-link"
      >
        {successMessage !== ""
          ? likedPosts && likedPosts.indexOf(post._id) !== -1
            ? likedIcon
            : unlikedIcon
          : post.liked
          ? likedIcon
          : unlikedIcon}
      </button>
      <div>
        {/* postId && post._id.toString() == postId.toString() && */}
        {postId && post._id.toString() === postId.toString() && likeCount >= 0
          ? likeCount
          : post.likedByUsers.length}
        Likes
      </div>
    </>
  );
}
