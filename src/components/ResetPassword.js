import React from "react";
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import renderFields from "./renderFields";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import { useSelector } from "react-redux";

const { renderInput } = renderFields;
const validate = values => {
  const errors = {};
  let { password, confirmPassword } = values;

  //password
  if (!password) errors.password = "Password is required";
  else if (password.length < 8)
    errors.password = "Password must be minimum 8 character long";

  //confirmPassword
  if (!confirmPassword) errors.confirmPassword = "Confirm Password is required";
  else if (confirmPassword !== password)
    errors.confirmPassword = "Password and Confirm Password are not matching";

  return errors;
};
let ResetPasswordForm = props => {
  const { handleSubmit, pristine, invalid } = props;

  const { errorMessage, successMessage, loading } = useSelector(
    ({ resetPass: { errorMessage, successMessage, loading } }) => ({
      errorMessage,
      successMessage,
      loading
    })
  );

  return (
    <div className="container">
      <Loading isLoading={loading} />

      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card  my-5">
            <div className="card-body">
              <h5 className="card-title text-center">Reset Password</h5>
              <h5 className="text-success text-center">{successMessage}</h5>
              <h5 className="text-danger text-center">{errorMessage}</h5>
              <form onSubmit={handleSubmit}>
                <Field
                  name="password"
                  label="Password"
                  component={renderInput}
                  type="password"
                />
                <Field
                  name="confirmPassword"
                  label="Confirm Password"
                  component={renderInput}
                  type="password"
                />
                <button
                  className="btn btn-lg btn-primary btn-block text-uppercase"
                  type="submit"
                  disabled={loading || pristine || invalid}
                >
                  Submit
                </button>
                <br />
                <div className="text-center">
                  <Link className="btn btn-link" to={`/login`}>
                    Back to login
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
function onSubmit(values, dispatch, props) {
  const code = props.match.params.code;
  dispatch(
    allActions.resetPassActions.resetPassword("/reset-password/" + code, values)
  );
}

ResetPasswordForm = reduxForm({
  form: "resetPasswordForm",
  onSubmit,
  validate
})(ResetPasswordForm);

export default ResetPasswordForm;
