import React from "react";
import { Link } from "react-router-dom";
import FollowButton from "./FollowButton";
import UnfollowButton from "./UnfollowButton";

export default function UserCard(props) {
  let { user, type, token, dispatch, showButton } = props;
  return (
    <div key={user._id} className="card mt-2">
      <div className="card-body">
        <div className="row">
          <div className="col-4">
            {user.profilePicPath ? (
              <img
                className="rounded-circle"
                height="80"
                width="80"
                alt="Profile Pic"
                src={`${process.env.REACT_APP_API_URL}${user.profilePicPath}`}
              />
            ) : (
              <img
                className="rounded-circle"
                height="80"
                width="80"
                alt="Profile Pic"
                src={`${process.env.REACT_APP_API_URL}/profilepic/unknown.jpeg`}
              />
            )}
            {showButton ? (
              <div className="mt-2">
                {type === "follow" ? (
                  <FollowButton user={user} token={token} dispatch={dispatch} />
                ) : (
                  <UnfollowButton
                    user={user}
                    token={token}
                    dispatch={dispatch}
                  />
                )}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="col-8">
            <Link
              className="card-title  btn btn-link"
              to={`/profile/${user._id}`}
            >
              <h5> {user.name}</h5>
            </Link>
            <p className="card-text">{user.location}</p>
            <p className="card-text">{user.email}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
