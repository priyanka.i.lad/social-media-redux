import React from "react";
import { useSelector, useDispatch } from "react-redux";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import { useEffect } from "react";
import UserCard from "./UserCard";

export default function UnfollowingUsers() {
  let dispatch = useDispatch();
  let { token, isLoggedIn, users, loading } = useSelector(
    ({
      login: { token, isLoggedIn },
      unFollowingUsers: { users, loading }
    }) => ({
      token,
      isLoggedIn,
      users,
      loading
    })
  );

  let classDisplay = isLoggedIn ? "block" : "none";
  useEffect(() => {
    if (isLoggedIn) {
      dispatch(
        allActions.unFollowingUsersActions.getUnfollowingUsers(
          "/user/unfollowing",
          token
        )
      );
    }
  }, [isLoggedIn, dispatch, token]);

  return (
    <div style={{ display: classDisplay }}>
      {loading ? (
        <Loading isLoading={loading} />
      ) : (
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="card bg-primary">
                <h5 className="card-title pt-3">People you may know</h5>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              {users &&
                users.map(user => (
                  <UserCard
                    key={user._id}
                    user={user}
                    token={token}
                    dispatch={dispatch}
                    type="follow"
                    showButton={true}
                  />
                ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
