import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import UserCard from "./UserCard";
export default function FollowingUsers(props) {
  let { loading, users, token } = useSelector(
    ({ followingUsers: { loading, users }, login: { token } }) => ({
      loading,
      users,
      token
    })
  );
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      allActions.followingUsersActions.getFollowingUsers(
        "/user/following",
        token
      )
    );
  }, [dispatch, token]);
  return loading ? (
    <Loading isLoading={loading} />
  ) : (
    <div className="container">
      <div className="row">
        {users &&
          users.map(user => (
            <div key={user._id} className="col-md-4">
              <UserCard
                key={user._id}
                user={user}
                token={token}
                dispatch={dispatch}
                type="unfollow"
                showButton={true}
              />
            </div>
          ))}
      </div>
    </div>
  );
}
