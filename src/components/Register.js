import React from "react";
import { useSelector } from "react-redux";
import { Field, reduxForm } from "redux-form";
import allActions from "../redux/allActions";
import { Link } from "react-router-dom";
import Loading from "./Loading";
import renderFields from "./renderFields";

const { renderInput } = renderFields;
const validate = values => {
  const errors = {};
  let { email, password, name, confirmPassword } = values;
  //email
  if (!email) errors.email = "Email is required";
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email))
    errors.email = "Please provide valid email address";

  //name
  if (!name) errors.name = "Name is required";

  //password
  if (!password) errors.password = "Password is required";
  else if (password.length < 8)
    errors.password = "Password must be minimum 8 character long";

  //confirmPassword
  if (!confirmPassword) errors.confirmPassword = "Confirm Password is required";
  else if (confirmPassword !== password)
    errors.confirmPassword = "Password and Confirm Password are not matching";

  return errors;
};

let RegisterForm = props => {
  const { handleSubmit, reset, pristine, invalid } = props;

  const { errorMessage, successMessage, loading } = useSelector(
    ({ register: { errorMessage, successMessage, loading } }) => ({
      errorMessage,
      successMessage,
      loading
    })
  );
  return (
    <div className="container">
      <Loading isLoading={loading} />

      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card  my-5">
            <div className="card-body">
              <h5 className="card-title text-center">Sign Up</h5>
              <h5 className="text-success text-center">{successMessage}</h5>
              <h5 className="text-danger text-center">{errorMessage}</h5>
              <form onSubmit={handleSubmit}>
                <Field
                  name="email"
                  component={renderInput}
                  label="Email"
                  type="email"
                />
                <Field
                  name="name"
                  component={renderInput}
                  type="text"
                  label="Display Name"
                />

                <Field
                  name="password"
                  label="Password"
                  component={renderInput}
                  type="password"
                />
                <Field
                  name="confirmPassword"
                  label="Confirm Password"
                  component={renderInput}
                  type="password"
                />
                <button
                  className="btn btn-md btn-primary text-uppercase"
                  type="submit"
                  disabled={loading || pristine || invalid}
                >
                  Register
                </button>
                <button
                  type="button"
                  onClick={reset}
                  className="btn btn-md btn-secondary text-uppercase ml-3"
                >
                  Reset
                </button>
                <br />

                <Link className="btn btn-link" to={`/login`}>
                  Back to Login
                </Link>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const onSubmit = (values, dispatch) => {
  dispatch(allActions.registerActions.registerUser("/register", values));
};

const onSubmitSuccess = (result, dispatch, props) => {
  dispatch(props.reset("registerForm"));
};

RegisterForm = reduxForm({
  form: "registerForm",
  onSubmit,
  onSubmitSuccess,
  validate
})(RegisterForm);

export default RegisterForm;
