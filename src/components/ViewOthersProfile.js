import React, { useState, useEffect } from "react";
import { Link, Switch } from "react-router-dom";
import OthersDetails from "./OthersDetails";
import OthersPost from "./OthersPost";
import PrivateRoute from "./PrivateRoute";
import OthersFollowers from "./OthersFollowers";
import OthersFollowing from "./OthersFollowing";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../redux/allActions";
import UserCard from "./UserCard";

export default function ViewOthersProfile(props) {
  let { userid } = props.match.params;
  const [activeTab, setActiveTab] = useState(0);
  let dispatch = useDispatch();

  let { user, token, _id: loggedInUserId } = useSelector(
    ({
      getOthersProfile: { user },
      login: {
        token,
        user: { _id }
      }
    }) => ({
      user,
      token,
      _id
    })
  );
  let handleTabSelect = tabIndex => {
    setActiveTab(tabIndex);
  };

  useEffect(() => {
    dispatch(
      allActions.getOthersProfileActions.getOtherUserProfile(
        `/user/profile/${userid}`,
        token
      )
    );
    return () => {
      //cleanup
    };
  }, [dispatch, userid, token]);
  let beingFollowed = isUserBeingFollowed(user, loggedInUserId);
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-5 text-center">
          <UserCard
            type={beingFollowed ? "unfollow" : "follow"}
            token={token}
            dispatch={dispatch}
            user={user}
            showButton={true}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <nav className="nav nav-pills nav-fill">
            <Link
              className={`nav-item nav-link ${activeTab === 0 ? "active" : ""}`}
              onClick={() => handleTabSelect(0)}
              to={`/profile/${userid}`}
            >
              Posts
            </Link>
            <Link
              className={`nav-item nav-link ${activeTab === 1 ? "active" : ""}`}
              onClick={() => handleTabSelect(1)}
              to={`/profile/${userid}/followers`}
            >
              Followers
            </Link>
            <Link
              className={`nav-item nav-link ${activeTab === 2 ? "active" : ""}`}
              onClick={() => handleTabSelect(2)}
              to={`/profile/${userid}/following`}
            >
              Following
            </Link>
          </nav>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          {beingFollowed ? (
            <OthersDetails>
              <Switch>
                <PrivateRoute
                  path="/profile/:userid/followers"
                  component={() => (
                    <OthersFollowers followers={user.followers} />
                  )}
                />
                <PrivateRoute
                  path="/profile/:userid/following"
                  component={() => (
                    <OthersFollowing following={user.following} />
                  )}
                />
                <PrivateRoute
                  exact
                  path="/profile/:userid"
                  component={() => <OthersPost posts={user.posts} />}
                />
              </Switch>
            </OthersDetails>
          ) : (
            <div>Follow this user to see other details</div>
          )}
        </div>
      </div>
    </div>
  );
}

function isUserBeingFollowed(user, loggedInUserId) {
  if (!user || !user.followers) return false;
  let followers = user.followers;
  for (let index = 0; index < followers.length; index++) {
    if (followers[index]._id === loggedInUserId) return true;
  }
  return false;
}
