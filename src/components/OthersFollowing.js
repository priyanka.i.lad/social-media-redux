import React from "react";
import UserCard from "./UserCard";

export default function OthersFollowing(props) {
  let { following } = props;
  return following && following.length > 0 ? (
    <div className="container">
      <div className="row">
        {following.map(user => (
          <div key={user._id} className="col-md-4">
            <UserCard key={user._id} user={user} showButton={false} />
          </div>
        ))}
      </div>
    </div>
  ) : (
    <div>This user is not following anyone </div>
  );
}
