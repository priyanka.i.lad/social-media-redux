import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import { Link } from "react-router-dom";

export default function AccountVerificationForm(props) {
  const code = props.match.params.code;

  const { errorMessage, successMessage, loading } = useSelector(
    ({ accVerify: { errorMessage, successMessage, loading } }) => ({
      errorMessage,
      successMessage,
      loading
    })
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      allActions.accVerifyActions.verifyAccount("/verify-account/" + code)
    );
  }, [dispatch, code]);
  return (
    <div className="container">
      <Loading isLoading={loading} />
      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card  my-5">
            <div className="card-body">
              <h5 className="card-title text-center">Account Verification</h5>
              <h5 className="text-danger text-center">{errorMessage}</h5>
              <h5 className="text-success text-center">{successMessage}</h5>

              <br />
              <div className="text-center">
                <Link className="btn btn-link" to={`/login`}>
                  Back to Login
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
