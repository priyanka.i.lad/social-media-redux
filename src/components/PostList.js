import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../redux/allActions";
import Post from "./Post";

let PostList = props => {
  const dispatch = useDispatch();
  const { token, posts } = useSelector(
    ({ postList: { posts }, login: { token } }) => ({
      token,
      posts
    })
  );
  useEffect(() => {
    dispatch(allActions.postListActions.getPosts("/user/post", token));
    return () => {
      dispatch(allActions.toggleLikePostActions.clearState());
    };
  }, [dispatch, token]);
  return (
    <div>{posts && posts.map(post => <Post key={post._id} post={post} />)}</div>
  );
};

export default PostList;
