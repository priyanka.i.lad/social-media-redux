import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import allActions from "../redux/allActions";

// let logout = dispatch => {
//   dispatch(allActions.login.logout);
// };

let Header = props => {
  const dispatch = useDispatch();

  const { name, profilePicPath, isLoggedIn } = useSelector(
    ({
      login: {
        user: { name, profilePicPath },
        isLoggedIn
      }
    }) => ({ name, profilePicPath, isLoggedIn })
  );

  const classDisplay = isLoggedIn ? "contents" : "none";
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        Social Media Application
      </Link>
      <div style={{ display: classDisplay }}>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
      </div>
      <div style={{ display: classDisplay }}>
        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarSupportedContent"
        >
          {/* <form className="form-inline my-2 mr-5 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
            >
              Search
            </button>
          </form> */}

          <ul className="navbar-nav mr-5">
            <li className="nav-item">
              <Link className="nav-link" to="/createpost">
                <i className="fa fa-upload" aria-hidden="true"></i> Create Post
              </Link>
            </li>
            <li className="nav-item dropdown">
              <button
                className="nav-link dropdown-toggle btn btn-link"
                id="navbarDropdown"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                {profilePicPath && (
                  <img
                    className="img-resonsive rounded-circle"
                    width="25"
                    height="25"
                    src={`${process.env.REACT_APP_API_URL}${profilePicPath}`}
                    alt="..."
                  ></img>
                )}

                {name}
              </button>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link className="dropdown-item" to="/editprofile">
                  My Profile
                </Link>
                <Link className="dropdown-item" to="/myposts">
                  My Posts
                </Link>
                <Link className="dropdown-item" to="/following">
                  Following
                </Link>
                <div className="dropdown-divider"></div>
                <Link
                  className="dropdown-item"
                  to="/login"
                  onClick={() => {
                    dispatch(allActions.loginActions.logout());
                  }}
                >
                  Logout
                </Link>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
