import React from "react";
import { reduxForm, Field } from "redux-form";
import renderFields from "./renderFields";
import { connect } from "react-redux";
import Loading from "./Loading";
import allActions from "../redux/allActions";

const { renderInput, renderTextArea } = renderFields;
const validate = values => {
  const errors = {};
  let { title } = values;

  if (!title) errors.title = "Title is required";
  else if (title.length > 50)
    errors.title = "Title is too long. Max length is 50";

  return errors;
};

let CreatePostForm = props => {
  const {
    handleSubmit,
    pristine,
    invalid,
    successMessage,
    errorMessage,
    loading
  } = props;

  return (
    <>
      <Loading isLoading={loading} />
      <div className="container">
        <div className="row">
          <div className="col">
            <h4 className="text-success">{successMessage}</h4>
            <h4 className="text-danger">{errorMessage}</h4>
            <form onSubmit={handleSubmit}>
              <h4>Create new post</h4>
              <Field
                name="title"
                label="Title"
                type="text"
                component={renderInput}
              />
              <Field
                name="description"
                label="Description"
                rows="4"
                cols="50"
                component={renderTextArea}
              />
              <button
                type="submit"
                className="btn btn-primary"
                disabled={invalid || pristine}
              >
                Post
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};
const onSubmit = (values, dispatch, props) => {
  dispatch(
    allActions.createPostActions.createNewPost(
      "/user/post/create",
      values,
      props.token
    )
  ).then(() => props.history.push("/"));
};

const mapStateToProps = (state, props) => {
  let { errorMessage, successMessage, loading } = state.editProfile;
  return {
    errorMessage: errorMessage,
    successMessage: successMessage,
    loading: loading,
    token: state.login.token
  };
};

CreatePostForm = connect(mapStateToProps)(
  reduxForm({
    form: "createPostForm",
    onSubmit,
    validate
  })(CreatePostForm)
);

export default CreatePostForm;
