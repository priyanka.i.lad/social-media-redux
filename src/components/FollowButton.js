import React from "react";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../redux/allActions";

export default function FollowButton(props) {
  let { user, token } = props;
  let dispatch = useDispatch();

  const { loading, successMessage, followingIds, followId } = useSelector(
    ({ follow: { loading, successMessage, followingIds, followId } }) => ({
      loading,
      successMessage,
      followingIds,
      followId
    })
  );

  let isNowFollowing =
    successMessage !== "" &&
    followingIds.length > 0 &&
    followingIds.indexOf(user._id) !== -1;

  return (
    <button
      disabled={isNowFollowing}
      onClick={() => {
        dispatch(
          allActions.followActions.followUser("/user/follow", user._id, token)
        );
      }}
      className="btn btn-primary"
    >
      {loading && followId === user._id
        ? "..."
        : isNowFollowing
        ? "Following"
        : "Follow"}
    </button>
  );
}
