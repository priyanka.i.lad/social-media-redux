import React from "react";
import formateDate from "../utils/formateDate";
import LikePost from "./LikePost";

export default function Post(props) {
  let { post } = props;
  let postedOn = formateDate(new Date(post.postedOn));

  return (
    <div className="card" key={post._id}>
      <div className="card-body">
        <div className="row">
          <div className="col-10">
            <h5 className="card-title">{post.title}</h5>
            <h6 className="card-subtitle mb-2 text-muted">{`Posted By: ${post.postedBy.name} on ${postedOn}`}</h6>
            <p className="card-text">{post.description}</p>
          </div>
          <div className="col-2 bg-light">
            <LikePost post={post} liked={post.liked} />
          </div>
        </div>
      </div>
    </div>
  );
}
