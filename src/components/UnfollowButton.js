import React from "react";
import allActions from "../redux/allActions";
import { useSelector } from "react-redux";

export default function UnfollowButton(props) {
  let { user, token, dispatch } = props;
  let { loading, unfollowingIds, unfollowId } = useSelector(
    ({ unfollow: { loading, unfollowingIds, unfollowId } }) => ({
      loading,
      unfollowingIds,
      unfollowId
    })
  );

  let isNowUnfollowing =
    unfollowingIds.length > 0 && unfollowingIds.indexOf(user._id) !== -1;
  return (
    <button
      disabled={isNowUnfollowing}
      onClick={() =>
        dispatch(
          allActions.unfollowActions.unfollowUser(
            "/user/unfollow",
            user._id,
            token
          )
        )
      }
      className="btn btn-primary"
    >
      {loading && unfollowId === user._id
        ? "..."
        : isNowUnfollowing
        ? "Unfollowing"
        : "Unfollow"}
    </button>
  );
}
