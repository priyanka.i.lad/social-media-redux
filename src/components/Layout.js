import React from "react";

import UnfollowingUsers from "./UnfollowingUsers";
import Header from "./Header";

export default function Layout(props) {
  return (
    <div>
      <Header />
      <div className="mt-3">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-8">{props.children}</div>

            <div className="col-lg-4 well text-center d-none d-lg-block">
              <UnfollowingUsers />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
