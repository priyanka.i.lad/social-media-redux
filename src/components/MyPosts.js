import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../redux/allActions";
import Post from "./Post";

let MyPosts = props => {
  const dispatch = useDispatch();
  const { token, posts } = useSelector(
    ({ login: { token }, myPostList: { posts } }) => ({
      token,
      posts
    })
  );

  useEffect(() => {
    dispatch(
      allActions.myPostListActions.getMyPosts("/user/post/myposts", token)
    );
  }, [dispatch, token]);
  return (
    <div>{posts && posts.map(post => <Post key={post._id} post={post} />)}</div>
  );
};

export default MyPosts;
