import React from "react";
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import renderFields from "./renderFields";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import { useSelector } from "react-redux";

const { renderInput } = renderFields;
const validate = values => {
  const errors = {};
  let { email } = values;

  //email
  if (!email) errors.email = "Email is required";
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email))
    errors.email = "Please provide valid email address";

  return errors;
};

let ForgotPasswordForm = props => {
  const { handleSubmit, pristine, invalid } = props;

  const { errorMessage, successMessage, loading } = useSelector(
    ({ forgotPass: { errorMessage, successMessage, loading } }) => ({
      errorMessage,
      successMessage,
      loading
    })
  );

  return (
    <div className="container">
      <Loading isLoading={loading} />

      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card  my-5">
            <div className="card-body">
              <h5 className="card-title text-center">Forgot Password</h5>
              <h5 className="text-success text-center">{successMessage}</h5>
              <h5 className="text-danger text-center">{errorMessage}</h5>
              <form onSubmit={handleSubmit}>
                <Field
                  name="email"
                  component={renderInput}
                  label="Email"
                  type="email"
                />

                <button
                  className="btn btn-lg btn-primary btn-block text-uppercase"
                  type="submit"
                  disabled={loading || pristine || invalid}
                >
                  Send
                </button>
                <br />
                <div className="text-center">
                  <Link className="btn btn-link" to={`/login`}>
                    Back to login
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
function onSubmit(values, dispatch) {
  dispatch(
    allActions.forgotPassActions.forgotPassword("/forgot-password", values)
  );
}

ForgotPasswordForm = reduxForm({
  form: "forgotPasswordForm",
  onSubmit,
  validate
})(ForgotPasswordForm);

export default ForgotPasswordForm;
