import React from "react";
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import renderFields from "./renderFields";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import { useSelector } from "react-redux";

const { renderInput } = renderFields;
const validate = values => {
  const errors = {};
  let { email, password } = values;

  //email
  if (!email) errors.email = "Email is required";
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email))
    errors.email = "Please provide valid email address";

  //password
  if (!password) errors.password = "Password is required";
  else if (password.length < 8)
    errors.password = "Password must be minimum 8 character long";

  return errors;
};

let LoginForm = props => {
  const { handleSubmit, pristine, invalid } = props;

  const { errorMessage, loading } = useSelector(
    ({ login: { errorMessage, loading } }) => ({
      errorMessage,
      loading
    })
  );

  return (
    <div className="container">
      <Loading isLoading={loading} />

      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card  my-5">
            <div className="card-body">
              <h5 className="card-title text-center">Sign In</h5>
              <h5 className="text-danger text-center">{errorMessage}</h5>
              <form onSubmit={handleSubmit}>
                <Field
                  name="email"
                  component={renderInput}
                  label="Email"
                  type="email"
                />

                <Field
                  name="password"
                  component={renderInput}
                  label="Password"
                  type="password"
                />

                <button
                  className="btn btn-lg btn-primary btn-block text-uppercase"
                  type="submit"
                  disabled={loading || pristine || invalid}
                >
                  Sign in
                </button>
                <br />
                <div className="text-center">
                  <Link className="btn btn-link" to={`/register`}>
                    New User? Click to Register
                  </Link>
                  <Link className="btn btn-link" to={`/forgotpassword`}>
                    Forgot Password? Click to reset it
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
function loginSubmit(values, dispatch, props) {
  dispatch(allActions.loginActions.login("/login", values))
    .then(() => props.history.push("/"))
    .catch(err => console.log(err));
}

LoginForm = reduxForm({
  form: "loginForm",
  onSubmit: loginSubmit,
  validate
})(LoginForm);

export default LoginForm;
