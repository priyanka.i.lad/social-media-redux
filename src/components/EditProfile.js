import React, { useEffect } from "react";
import { Field, reduxForm } from "redux-form";
import { useDispatch, connect } from "react-redux";
import allActions from "../redux/allActions";
import Loading from "./Loading";
import renderFields from "./renderFields";
import "../index.css";

const { renderInput, renderRadioInput, renderSelect } = renderFields;
const validate = values => {
  const errors = {};
  let { name, education, age, location, gender } = values;

  //name
  if (!name) errors.name = "Name is required";

  //education
  if (!education) errors.education = "Education is required";

  //age
  if (!age) errors.age = "Age is required";
  else if (age.toString().indexOf(".") !== -1 || age < 10 || age > 105) {
    errors.age = "Please enter valid age";
  }

  //location
  if (!location) errors.location = "Location is required";

  //gender
  if (!gender) errors.gender = "Gender is required";

  return errors;
};

function EditProfileForm(props) {
  const { handleSubmit, reset, token, loading, errorMessage, viewMode } = props;

  const dispatch = useDispatch();
  const image_style = {
    width: "150px",
    height: "150px",
    borderRadius: "50%"
  };

  let editForm = (
    <form onSubmit={handleSubmit} encType="multipart/form-data">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h3 className="text-danger">{errorMessage}</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center">
            <div className="image-upload">
              <label htmlFor="profilePic">
                <img
                  alt="Profile Pic"
                  style={image_style}
                  src={`${process.env.REACT_APP_API_URL}${props.imageFilePath}`}
                />
              </label>
              <label>{props.imageFile && props.imageFile.name}</label>
              <input
                id="profilePic"
                name="profilePic"
                onChange={e =>
                  dispatch(
                    allActions.editProfileActions.uploadImage(e.target.files[0])
                  )
                }
                type="file"
              />
            </div>
            <Field
              name="name"
              extraCssClass=" w-25 mx-auto"
              label="Display Name"
              component={renderInput}
              type="text"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <Field
              name="email"
              component={renderInput}
              label="Email"
              type="email"
              style={{ pointerEvents: "none", backgroundColor: "lightgray" }}
            />

            <Field
              name="age"
              label="Age"
              component={renderInput}
              type="number"
            />
            <Field
              name="location"
              label="Location"
              component={renderInput}
              type="text"
            />
          </div>
          <div className="col-6">
            <Field
              component={renderRadioInput}
              label="Gender"
              name="gender"
              options={{
                male: { label: "Male", selected: false },
                female: { label: "Female", selected: false },
                other: { label: "Other", selected: false }
              }}
            />
            <Field name="education" label="Education" component={renderSelect}>
              <option value="">Select Education</option>
              <option value="phd">PhD</option>
              <option value="masters">Masters Degree</option>
              <option value="bachelor">Bachelor Degree</option>
              <option value="10th">10th</option>
              <option value="12th">12th</option>
              <option value="other">Other</option>
            </Field>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center">
            <button type="submit" className="btn btn-success">
              Update
            </button>
            <button
              type="button"
              onClick={reset}
              className="m-1 btn btn-secondary"
            >
              Reset
            </button>
            <button
              type="button"
              onClick={() =>
                dispatch(allActions.editProfileActions.changeViewMode("view"))
              }
              className="m-1 btn btn-secondary"
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </form>
  );

  let {
    email = "",
    name = "",
    gender = "",
    education = "",
    age = "",
    location = "",
    profilePicPath = ""
  } = props.initialValues;

  let viewForm = (
    <div className="container">
      <div className="row">
        <div className="col-12 text-center">
          <div className="align-items-center">
            <img
              style={image_style}
              alt="Profile pic"
              src={`${process.env.REACT_APP_API_URL}${profilePicPath}`}
            />
            <h2 className="ml-2">{name}</h2>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-6">
          <div className="form-group">
            <label>Email</label>
            <label className="form-control">{email}</label>
          </div>

          <div className="form-group">
            <label>Age</label>
            <label className="form-control">{age}</label>
          </div>
          <div className="form-group">
            <label>Location</label>
            <label className="form-control">{location}</label>
          </div>
        </div>
        <div className="col-6">
          <div className="form-group">
            <label>Gender</label>
            <label className="form-control">{gender}</label>
          </div>
          <div className="form-group">
            <label>Education</label>
            <label className="form-control">{education}</label>
          </div>
        </div>
      </div>
    </div>
  );
  useEffect(() => {
    dispatch(
      allActions.editProfileActions.getUserProfile("/user/profile", token)
    );
    return () => {
      dispatch(allActions.editProfileActions.changeViewMode("view"));
    };
  }, [dispatch, token]);
  return loading ? (
    <Loading isLoading={loading} />
  ) : (
    <>
      <div className="d-inline-flex">
        <h2 className="h2"> Profile Settings</h2>
        <button
          style={{ visibility: viewMode === "view" ? "visible" : "hidden" }}
          className="btn btn-outline-primary ml-3"
          title="Edit Profile"
          onClick={() =>
            dispatch(allActions.editProfileActions.changeViewMode("edit"))
          }
        >
          <i className="fa fa-pencil" aria-hidden="true"></i>
        </button>
      </div>

      {viewMode === "edit" ? editForm : viewForm}
    </>
  );
}

const onSubmit = (values, dispatch, props) => {
  let { token, imageFile } = props;
  if (imageFile) values.profilePic = imageFile;
  dispatch(
    allActions.editProfileActions.editUserProfile(
      "/user/profile",
      values,
      token
    )
  );
};

const mapStateToProps = (state, props) => {
  let {
    user,
    imageFile,
    imageFilePath,
    errorMessage,
    loading,
    viewMode
  } = state.editProfile;
  return {
    initialValues: user,
    imageFile: imageFile,
    imageFilePath: imageFilePath,
    token: state.login.token,
    errorMessage: errorMessage,
    loading: loading,
    viewMode: viewMode
  };
};

export default connect(mapStateToProps)(
  reduxForm({
    form: "editProfile",
    enableReinitialize: true,
    onSubmit,
    validate
  })(EditProfileForm)
);
