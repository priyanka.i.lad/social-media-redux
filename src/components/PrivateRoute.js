import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useSelector } from "react-redux";

const PrivateRoute = ({ component: Component, ...rest }) => {
  //console.log(useSelector(state => state));
  const isUserLoggedIn = useSelector(state => state.login.isLoggedIn);
  return (
    <Route
      {...rest}
      render={props =>
        isUserLoggedIn ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};

export default PrivateRoute;
