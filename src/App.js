import React from "react";
import "./App.css";
import RegisterForm from "./components/Register";
import LoginForm from "./components/Login";
import { Provider } from "react-redux";
import store from "./redux/store";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import AccountVerificationForm from "./components/AccountVerification";
import ForgotPasswordForm from "./components/ForgotPassword";
import ResetPasswordForm from "./components/ResetPassword";
import CreatePostForm from "./components/CreatePost";
import EditProfileForm from "./components/EditProfile";
import FollowingUsers from "./components/FollowingUsers";
import MyPosts from "./components/MyPosts";
import Layout from "./components/Layout";
import PostList from "./components/PostList";
import ViewOthersProfile from "./components/ViewOthersProfile";
import NotFound from "./components/NotFound";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Layout>
          <Switch>
            <Route path="/login" component={LoginForm} />
            <Route path="/register" component={RegisterForm} />
            <Route
              path="/verifyaccount/:code"
              component={AccountVerificationForm}
            />
            <Route path="/forgotpassword" component={ForgotPasswordForm} />
            <Route path="/resetpassword/:code" component={ResetPasswordForm} />
            <PrivateRoute path="/createpost" component={CreatePostForm} />
            <PrivateRoute path="/editprofile" component={EditProfileForm} />
            <PrivateRoute path="/following" component={FollowingUsers} />
            <PrivateRoute path="/myposts" component={MyPosts} />
            <PrivateRoute path="/" exact component={PostList} />
            <PrivateRoute
              path="/profile/:userid"
              component={ViewOthersProfile}
            />
            <Route path="*" component={NotFound} />
          </Switch>
        </Layout>
      </Router>
    </Provider>
  );
}

export default App;
