export const REQUEST = "REQUEST";
export const SUCCESS = "SUCCESS";
export const FAILURE = "FAILURE";

export const doRequest = followId => {
  return {
    type: REQUEST,
    payload: followId
  };
};
export const processSuccess = successResponse => {
  return {
    type: SUCCESS,
    payload: successResponse
  };
};

export const processFailure = errorResponse => {
  return {
    type: FAILURE,
    payload: errorResponse
  };
};
