import { REQUEST, SUCCESS, FAILURE } from "./unFollowingUsersActions";
const initial_state = {
  loading: false,
  users: [],
  errorMessage: ""
};

const unFollowingUsersReducer = (state = initial_state, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        loading: false,
        users: payload.data.users,
        errorMessage: ""
      };
    case FAILURE:
      return {
        loading: false,
        users: [],
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default unFollowingUsersReducer;
