// Action Types
export const REQUEST = "UNFOLLOWING_USERS_REQUEST";
export const SUCCESS = "UNFOLLOWING_USERS_SUCCESS";
export const FAILURE = "UNFOLLOWING_USERS_FAILURE";

const getUnfollowingUsers = (url, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default { getUnfollowingUsers };
