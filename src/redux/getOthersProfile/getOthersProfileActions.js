export const REQUEST = "GET_PROFILE_REQUEST";
export const SUCCESS = "GET_PROFILE_SUCCESS";
export const FAILURE = "GET_PROFILE_FAILURE";

const getOtherUserProfile = (url, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default {
  getOtherUserProfile
};
