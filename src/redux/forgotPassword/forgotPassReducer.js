import { REQUEST, SUCCESS, FAILURE } from "./forgotPassActions";
const initialState = {
  loading: false,
  successMessage: "",
  errorMessage: ""
};

const forgotPassReducer = (state = initialState, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        loading: false,
        successMessage: payload.data.message,
        errorMessage: ""
      };
    case FAILURE:
      return {
        loading: false,
        successMessage: "",
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default forgotPassReducer;
