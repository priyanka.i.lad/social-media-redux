import { createStore, applyMiddleware } from "redux";
import rootReducer from "./rootReducer";
// import logger from "redux-logger";
//import { composeWithDevTools } from "redux-devtools-extension";
import { multiClientMiddleware } from "redux-axios-middleware";
import thunkMiddleware from "redux-thunk";
import clients from "./clients";
import middlewareConfig from "./middlewareConfig";

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware,
    multiClientMiddleware(clients, middlewareConfig)
  )
);

export default store;
