export const REQUEST = "FOLLOWING_USERS_REQUEST";
export const SUCCESS = "FOLLOWING_USERS_SUCCESS";
export const FAILURE = "FOLLOWING_USERS_FAILURE";

const getFollowingUsers = (url, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default {
  getFollowingUsers
};
