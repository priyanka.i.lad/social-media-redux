import { REQUEST, SUCCESS, FAILURE } from "./followingUsersActions";
const initial_state = {
  loading: false,
  users: [],
  errorMessage: ""
};

const followingUsersReducer = (state = initial_state, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        loading: false,
        users: payload.data.users,
        errorMessage: ""
      };
    case FAILURE:
      return {
        loading: false,
        users: [],
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default followingUsersReducer;
