export const REQUEST = "UNFOLLOW_REQUEST";
export const SUCCESS = "UNFOLLOW_SUCCESS";
export const FAILURE = "UNFOLLOW_FAILURE";

const unfollowUser = (url, unfollowId, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        data: { unfollowId: unfollowId },
        method: "patch",
        headers: {
          Authorization: `Bearer ${token}`
        }
      },
      options: {
        onSuccess({ getState, dispatch, response }) {
          dispatch({ type: SUCCESS, payload: unfollowId });
        }
      }
    }
  };
};

export default {
  unfollowUser
};
