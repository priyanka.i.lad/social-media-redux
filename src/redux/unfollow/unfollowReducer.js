import { REQUEST, SUCCESS, FAILURE } from "./unfollowActions";
const initial_state = {
  loading: false,
  sucessMessage: "",
  errorMessage: "",
  unfollowId: "",
  unfollowingIds: []
};

const unfollowReducer = (state = initial_state, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        loading: false,
        unfollowId: payload,
        errorMessage: "",
        unfollowingIds: [...state.unfollowingIds, payload]
      };
    case FAILURE:
      return {
        loading: false,
        sucessMessage: "",
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default unfollowReducer;
