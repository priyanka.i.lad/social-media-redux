// Action Types
export const REQUEST = "LOGIN_REQUEST";
export const SUCCESS = "LOGIN_SUCCESS";
export const FAILURE = "LOGIN_FAILURE";
export const LOGOUT = "LOGOUT";

// Actions
const logout = () => {
  return {
    type: LOGOUT
  };
};

const login = (url, data) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "post",
        data
      }
    }
  };
};

export default {
  login,
  logout
};
