import { REQUEST, SUCCESS, FAILURE, LOGOUT } from "./loginActions";

const initialState = {
  loading: false,
  user: {},
  token: "",
  isLoggedIn: false,
  errorMessage: ""
};

const loginReducer = (state = initialState, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      let { user, token } = payload.data;
      return {
        loading: false,
        user: user,
        isLoggedIn: true,
        token: token,
        errorMessage: ""
      };
    case FAILURE:
      return {
        loading: false,
        user: {},
        isLoggedIn: false,
        errorMessage: error.data
      };
    case LOGOUT:
      return {
        ...state,
        token: "",
        user: {},
        isLoggedIn: false
      };
    default:
      return state;
  }
};

export default loginReducer;
