import {
  EDIT_REQUEST,
  EDIT_SUCCESS,
  EDIT_FAILURE,
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILURE,
  UPLOAD_IMAGE,
  CHANGE_VIEW_MODE
} from "./editProfileActionTypes";

const initialState = {
  loading: false,
  successMessage: "",
  errorMessage: "",
  imageFile: null,
  imageFilePath: "",
  user: {},
  viewMode: "view"
};

const editProfileReducer = (state = initialState, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case EDIT_REQUEST:
      return {
        ...state,
        loading: true
      };

    case CHANGE_VIEW_MODE:
      return {
        ...state,
        viewMode: action.payload
      };
    case EDIT_SUCCESS:
      return {
        loading: false,
        successMessage: payload.data.message,
        errorMessage: "",
        imageFilePath: payload.data.user.profilePicURL,
        viewMode: "view",
        user: payload.data.user
      };
    case EDIT_FAILURE:
      return {
        loading: false,
        successMessage: "",
        errorMessage: error.data,
        viewMode: "edit"
      };
    case GET_PROFILE_REQUEST:
      return {
        ...state,
        loading: true
      };
    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        successMessage: "",
        errorMessage: "",
        user: payload.data.user,
        imageFilePath: payload.data.user.profilePicPath
      };
    case GET_PROFILE_FAILURE:
      return {
        loading: false,
        successMessage: "",
        user: {},
        errorMessage: error.data
      };
    case UPLOAD_IMAGE:
      return {
        ...state,
        imageFile: action.payload
      };
    default:
      return state;
  }
};

export default editProfileReducer;
