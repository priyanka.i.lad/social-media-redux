import {
  EDIT_REQUEST,
  EDIT_SUCCESS,
  EDIT_FAILURE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_REQUEST,
  GET_PROFILE_FAILURE,
  UPLOAD_IMAGE,
  CHANGE_VIEW_MODE
} from "./editProfileActionTypes";

const changeViewMode = viewMode => {
  return {
    type: CHANGE_VIEW_MODE,
    payload: viewMode
  };
};

const uploadImage = imageFile => {
  return { type: UPLOAD_IMAGE, payload: imageFile };
};

const getUserProfile = (url, token) => {
  return {
    types: [GET_PROFILE_REQUEST, GET_PROFILE_SUCCESS, GET_PROFILE_FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

const editUserProfile = (url, data, token) => {
  const formData = getFormData(data);
  return {
    types: [EDIT_REQUEST, EDIT_SUCCESS, EDIT_FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "put",
        data: formData,
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "multipart/form-data"
        }
      }
    }
  };
  // const formData = getFormData(data);
  // return dispatch => {
  //   let base_url = process.env.REACT_APP_API_URL;
  //   dispatch(editRequest());
  //   axios
  //     .put(base_url + "/user/profile", formData, {
  //       headers: {
  //         "Content-Type": "multipart/form-data", //"application/x-www-form-urlencoded",
  //         Authorization: `Bearer ${token}`
  //       }
  //     })
  //     .then(response => dispatch(editSuccess(response.data)))
  //     .catch(err => dispatch(editFailure(err.message)));
  // };
};

function getFormData(object) {
  let formData = new FormData();
  for (var key in object) {
    formData.append(key, object[key]);
  }
  return formData;
}

export default {
  editUserProfile,
  getUserProfile,
  uploadImage,
  changeViewMode
};
