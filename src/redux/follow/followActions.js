export const REQUEST = "FOLLOW_REQUEST";
export const SUCCESS = "FOLLOW_SUCCESS";
export const FAILURE = "FOLLOW_FAILURE";

const followUser = (url, followId, token) => {
  console.log(token);
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        data: { followId: followId },
        method: "patch",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default {
  followUser
};
