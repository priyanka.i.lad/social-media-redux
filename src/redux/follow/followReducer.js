import { REQUEST, SUCCESS, FAILURE } from "./followActions";
const initial_state = {
  loading: false,
  successMessage: "",
  errorMessage: "",
  followingIds: [],
  followId: ""
};

const followReducer = (state = initial_state, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      console.log(payload);
      return {
        ...state,
        loading: true,
        followId: payload
      };
    case SUCCESS:
      console.log(payload.data);
      return {
        loading: false,
        successMessage: payload.data.message,
        errorMessage: "",
        followingIds: payload.data.followingIds
      };
    case FAILURE:
      return {
        loading: false,
        successMessage: "",
        errorMessage: error.data,
        followingIds: []
      };
    default:
      return state;
  }
};

export default followReducer;
