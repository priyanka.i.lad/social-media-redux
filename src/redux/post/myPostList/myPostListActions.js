export const REQUEST = "ALLPOSTS_REQUEST";
export const SUCCESS = "ALLPOSTS_SUCCESS";
export const FAILURE = "ALLPOSTS_FAILURE";

const getMyPosts = (url, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default {
  getMyPosts
};
