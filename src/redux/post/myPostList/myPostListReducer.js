import { REQUEST, SUCCESS, FAILURE } from "./myPostListActions";

const initialState = {
  loading: false,
  posts: [],
  errorMessage: ""
};

const myPostListReducer = (state = initialState, action) => {
  let { type, payload, error } = action;

  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        posts: payload.data.posts,
        loading: false,
        errorMessage: ""
      };
    case FAILURE:
      return {
        posts: [],
        loading: false,
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default myPostListReducer;
