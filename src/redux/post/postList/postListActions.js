export const REQUEST = "ALL_POSTS_REQUEST";
export const SUCCESS = "ALL_POSTS_SUCCESS";
export const FAILURE = "ALL_POSTS_FAILURE";

const getPosts = (url, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default {
  getPosts
};
