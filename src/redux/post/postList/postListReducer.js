import { REQUEST, SUCCESS, FAILURE } from "./postListActions";

const initialState = {
  loading: false,
  posts: [],
  errorMessage: ""
};

const postListReducer = (state = initialState, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        posts: payload.data.posts,
        loading: false,
        errorMessage: ""
      };
    case FAILURE:
      return {
        posts: [],
        loading: false,
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default postListReducer;
