export const REQUEST = "CRESTE_POST_REQUEST";
export const SUCCESS = "CREATE_POST_SUCCESS";
export const FAILURE = "CREATE_POST_FAILURE";

const createNewPost = (url, data, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "post",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    }
  };
};

export default {
  createNewPost
};
