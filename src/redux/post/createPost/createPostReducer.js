import { REQUEST, SUCCESS, FAILURE } from "./createPostActions";

const initialState = {
  loading: false,
  successMessage: "",
  errorMessage: ""
};

const createPostReducer = (state = initialState, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      return {
        loading: false,
        successMessage: payload.data,
        errorMessage: ""
      };
    case FAILURE:
      return {
        loading: false,
        successMessage: "",
        errorMessage: error.data
      };
    default:
      return state;
  }
};

export default createPostReducer;
