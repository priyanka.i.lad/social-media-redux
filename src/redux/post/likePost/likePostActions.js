export const REQUEST = "LIKE_POST_REQUEST";
export const SUCCESS = "LIKE_POST_SUCCESS";
export const FAILURE = "LIKE_POST_FAILURE";
export const CLEAR = "CLEAR";

const clearState = () => {
  return {
    type: CLEAR
  };
};

const toggleLikePost = (url, postId, token) => {
  return {
    types: [REQUEST, SUCCESS, FAILURE],
    payload: {
      client: "default",
      request: {
        url: url,
        method: "put",
        data: { postId },
        headers: {
          Authorization: `Bearer ${token}`
        }
      },
      options: {
        onSuccess({ getState, dispatch, response }) {
          dispatch({ type: SUCCESS, payload: { ...response.data, postId } });
        }
      }
    }
  };
};

export default {
  toggleLikePost,
  clearState
};
