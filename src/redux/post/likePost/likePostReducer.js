import { REQUEST, SUCCESS, FAILURE, CLEAR } from "./likePostActions";

const initialState = {
  loading: false,
  likedSuccess: "",
  errorMessage: "",
  likedPosts: [],
  likeCount: -1,
  postId: ""
};

const toggleLikePostReducer = (state = initialState, action) => {
  let { type, payload, error } = action;
  switch (type) {
    case REQUEST:
      return {
        ...state,
        loading: true,
        postId: action.payload
      };
    case SUCCESS:
      let { likedPosts, likeCount, message, postId } = payload;
      return {
        ...state,
        likedSuccess: message,
        loading: false,
        errorMessage: "",
        likedPosts: likedPosts,
        likeCount: likeCount,
        postId: postId
      };
    case FAILURE:
      return {
        likedSuccess: "",
        loading: false,
        errorMessage: error.data,
        likedPosts: [],
        likeCount: 0
      };
    case CLEAR:
      return initialState;
    default:
      return state;
  }
};

export default toggleLikePostReducer;
